<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $primaryKey = 'orderNumber';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'orderNumber',
        'orderDate',
        'requiredDate',
        'shippedDate',
        'status',
        'comments',
        'customerNumber'
    ];

    public function details()
    {
        return $this->hasOne(OrderDetail::class,'orderNumber','orderNumber');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class,'customerNumber','customerNumber');
    }
}
