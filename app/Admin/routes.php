<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');

    $router->resource('/customers', CustomerController::class);
    $router->resource('/employees', EmployeeController::class);
    $router->resource('/orders', OrderController::class);
    $router->resource('/payments', PaymentController::class);
    $router->resource('/products', ProductController::class);
    $router->resource('/product-lines', ProductLineController::class);
    $router->resource('offices', OfficeController::class);

});
