<?php

namespace App\Admin\Controllers;

use App\Models\Payment;
use App\Models\Customer;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PaymentController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Payment';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Payment());

        $customers = Customer::selectRaw('customerNumber AS id, customerName AS name')->get()->pluck('name','id')->toArray();

        $grid->column('customer.customerName', __('Customer'))->sortable();
        $grid->column('checkNumber', __('Check Number'))->sortable();
        $grid->column('paymentDate', __('Payment Date'))->sortable();
        $grid->column('amount', __('Amount'))->view('admin.partials.money')->sortable();

        // disable actions
        $grid->disableCreateButton();
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();
        $grid->disableActions();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Payment::findOrFail($id));

        $show->field('customerNumber', __('Customer #'));
        $show->field('checkNumber', __('Check Number'));
        $show->field('paymentDate', __('Payment Date'));
        $show->field('amount', __('Amount'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Payment());

        $customers = Customer::selectRaw('customerNumber AS id, customerName AS name')->get()->pluck('name','id')->toArray();

        $form->select('customerNumber', __('Customer #'))->options($customers)->required('rules');
        $form->text('checkNumber', __('CheckNumber'))->required('rules');
        $form->date('paymentDate', __('PaymentDate'))->default(date('Y-m-d'))->required('rules');
        $form->currency('amount', __('Amount'))->required('rules');

        return $form;
    }
}
