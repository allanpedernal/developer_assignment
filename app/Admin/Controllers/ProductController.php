<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());

        $grid->column('productCode', __('Code'))->sortable();
        $grid->column('productName', __('Name'))->sortable();
        $grid->column('productLine', __('Line'))->sortable();
        $grid->column('productScale', __('Scale'))->sortable();
        $grid->column('productVendor', __('Vendor'))->sortable();
        $grid->column('productDescription', __('Desciption'))->sortable();
        $grid->column('quantityInStock', __('Stock'))->sortable();
        $grid->column('buyPrice', __('Price'))->view('admin.partials.money')->sortable();
        $grid->column('MSRP', __('MSRP'))->view('admin.partials.money')->sortable();

        // disable actions
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('productCode', __('Code'));
        $show->field('productName', __('Name'));
        $show->field('productLine', __('Line'));
        $show->field('productScale', __('Scale'));
        $show->field('productVendor', __('Vendor'));
        $show->field('productDescription', __('Desciption'));
        $show->field('quantityInStock', __('Stock'));
        $show->field('buyPrice', __('Price'));
        $show->field('MSRP', __('MSRP'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());

        $form->hidden('productCode', __('Code'));
        $form->text('productName', __('Name'))->required('rules');
        $form->text('productLine', __('Line'))->required('rules');
        $form->text('productScale', __('Scale'))->required('rules');
        $form->text('productVendor', __('Vendor'))->required('rules');
        $form->textarea('productDescription', __('Desciption'))->required('rules');
        $form->number('quantityInStock', __('Stock'))->required('rules');
        $form->currency('buyPrice', __('Price'))->required('rules');
        $form->currency('MSRP', __('MSRP'))->required('rules');

        // before saving
        $form->saving(function (Form $form) {
            $form->productCode = \Str::random(15);
        });

        return $form;
    }
}
