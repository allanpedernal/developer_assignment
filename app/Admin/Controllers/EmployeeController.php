<?php

namespace App\Admin\Controllers;

use App\Models\Employee;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class EmployeeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Employees';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Employee());
        $grid->model()->orderBy('employeeNumber','desc');

        $grid->column('employeeNumber', __('Employee #'))->sortable();
        $grid->column('firstName', __('First Name'))->sortable();
        $grid->column('lastName', __('Last Name'))->sortable();
        $grid->column('extension', __('Extension'))->sortable();
        $grid->column('email', __('Email'))->sortable();
        $grid->column('officeCode', __('Office Code'))->sortable();
        $grid->column('report_to.firstName', __('Reports To'))->sortable();
        $grid->column('jobTitle', __('Job Title'))->sortable();

        // disable actions
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Employee::findOrFail($id));

        $show->field('employeeNumber', __('Employee #'));
        $show->field('firstName', __('First Name'));
        $show->field('lastName', __('Last Name'));
        $show->field('extension', __('Extension'));
        $show->field('email', __('Email'));
        $show->field('officeCode', __('Office Code'));
        $show->field('reportsTo', __('Reports To'));
        $show->field('jobTitle', __('Job Title'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Employee());

        $employees = Employee::selectRaw('employeeNumber AS id, CONCAT_WS(" ", firstName, lastName) AS name')->get()->pluck('name','id')->toArray();

        $form->hidden('employeeNumber', __('Employee #'));
        $form->text('firstName', __('FirstName'))->required('rules');
        $form->text('lastName', __('LastName'))->required('rules');
        $form->text('extension', __('Extension'))->required('rules');
        $form->email('email', __('Email'))->required('rules');
        $form->text('officeCode', __('Office Code'))->required('rules');
        $form->select('reportsTo', __('Reports To'))->options($employees)->required('rules');
        $form->text('jobTitle', __('Job Title'))->required('rules');

        // before saving
        $form->saving(function (Form $form) {
            $form->employeeNumber = Employee::max('employeeNumber') + 1;
        });

        return $form;
    }
}
