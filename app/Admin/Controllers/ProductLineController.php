<?php

namespace App\Admin\Controllers;

use App\Models\ProductLine;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProductLineController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'ProductLine';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductLine());

        $grid->column('productLine', __('Line'))->editable()->sortable();
        $grid->column('textDescription', __('Tex tDescription'))->editable()->sortable();
        $grid->column('htmlDescription', __('Html Description'))->editable()->sortable();

        // disable actions
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProductLine::findOrFail($id));

        $show->field('productLine', __('Line'));
        $show->field('textDescription', __('TextDescription'));
        $show->field('htmlDescription', __('HtmlDescription'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ProductLine());

        $form->text('productLine', __('Line'));
        $form->text('textDescription', __('Text Description'));
        $form->textarea('htmlDescription', __('Html Description'));

        return $form;
    }
}
