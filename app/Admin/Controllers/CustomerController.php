<?php

namespace App\Admin\Controllers;

use App\Models\Customer;
use App\Models\Employee;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CustomerController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Customers';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Customer());
        $grid->model()->orderBy('customerNumber','desc');

        $grid->column('customerNumber', __('Customer #'))->sortable();
        $grid->column('contactFirstName', __('First Name'))->sortable();
        $grid->column('contactLastName', __('Last Name'))->sortable();
        $grid->column('phone', __('Phone'))->sortable();
        $grid->column('addressLine1', __('Address'))->sortable();
        $grid->column('city', __('City'))->sortable();
        $grid->column('state', __('State'))->sortable();
        $grid->column('postalCode', __('Zip Code'))->sortable();
        $grid->column('country', __('Country'))->sortable();
        $grid->column('sales_rep.firstName', __('Sales Rep'))->sortable();
        $grid->column('creditLimit', __('Credit Limit'))->view('admin.partials.money')->sortable();

        // disable actions
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Customer::findOrFail($id));

        $show->field('customerNumber', __('Customer #'));
        $show->field('customerName', __('Name'));
        $show->field('phone', __('Phone'));
        $show->field('addressLine1', __('Address'));
        $show->field('city', __('City'));
        $show->field('state', __('State'));
        $show->field('postalCode', __('Zip Code'));
        $show->field('country', __('Country'));
        $show->field('salesRepEmployeeNumber', __('Sales Rep'));
        $show->field('creditLimit', __('Credit Limit'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Customer());

        $employees = Employee::selectRaw('employeeNumber AS id, CONCAT_WS(" ", firstName, lastName) AS name')->get()->pluck('name','id')->toArray();
        
        $form->hidden('customerNumber', __('Customer #'));
        $form->hidden('customerName', __('Name'));
        $form->text('contactFirstName', __('Firstname'))->rules('required');
        $form->text('contactLastName', __('Lastname'))->rules('required');
        $form->mobile('phone', __('Phone'))->rules('required');
        $form->text('addressLine1', __('Address 1'))->rules('required');
        $form->text('addressLine2', __('Address 2'));
        $form->text('country', __('Country'))->rules('required');
        $form->text('state', __('State'))->rules('required');
        $form->text('city', __('City'))->rules('required');
        $form->text('postalCode', __('Zip Code'))->rules('required');
        $form->select('salesRepEmployeeNumber', __('Sales Rep'))->options($employees)->rules('required');
        $form->currency('creditLimit', __('Credit Limit'))->rules('required');

        // before saving
        $form->saving(function (Form $form) {
            $form->customerNumber = Customer::max('customerNumber') + 1;
            $form->customerName = $form->contactFirstName . ' ' . $form->contactLastName;
        });

        return $form;
    }
}
