<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use App\Models\Customer;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class OrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Orders';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order());
        $grid->model()->orderBy('orderNumber','desc');

        $grid->column('orderNumber', __('Order #'))->sortable();
        $grid->column('orderDate', __('Order Date'))->sortable();
        $grid->column('requiredDate', __('Required Date'))->sortable();
        $grid->column('shippedDate', __('Shipped Date'))->sortable();
        $grid->column('status', __('Status'))->sortable();
        $grid->column('comments', __('Comments'))->sortable();
        $grid->column('customer.customerName', __('Customer'))->sortable();

        // disable actions
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Order::findOrFail($id));

        $show->field('orderNumber', __('Order #'));
        $show->field('orderDate', __('Order Date'));
        $show->field('requiredDate', __('Required Date'));
        $show->field('shippedDate', __('Shipped Date'));
        $show->field('status', __('Status'));
        $show->field('comments', __('Comments'));
        $show->field('customerNumber', __('Customer #'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order());

        $customers = Customer::selectRaw('customerNumber AS id, customerName AS name')->get()->pluck('name','id')->toArray();

        $form->hidden('orderNumber', __('OrderNumber'))->required('rules');
        $form->date('orderDate', __('Order Date'))->default(date('Y-m-d'))->required('rules');
        $form->date('requiredDate', __('Required Date'))->default(date('Y-m-d'))->required('rules');
        $form->date('shippedDate', __('Shipped Date'))->default(date('Y-m-d'))->required('rules');
        $form->text('status', __('Status'))->required('rules');
        $form->textarea('comments', __('Comments'))->required('rules');
        $form->select('customerNumber', __('Customer'))->options($customers)->required('rules');

        // before saving
        $form->saving(function (Form $form) {
            $form->orderNumber = Order::max('orderNumber') + 1;
        });

        return $form;
    }
}
