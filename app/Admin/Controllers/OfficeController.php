<?php

namespace App\Admin\Controllers;

use App\Models\Office;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class OfficeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Offices';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Office());
        $grid->model()->orderBy('officeCode','desc');

        $grid->column('officeCode', __('Code'))->sortable();
        $grid->column('city', __('City'))->sortable();
        $grid->column('phone', __('Phone'))->sortable();
        $grid->column('addressLine1', __('Address'))->sortable();
        $grid->column('state', __('State'))->sortable();
        $grid->column('country', __('Country'))->sortable();
        $grid->column('postalCode', __('Zip Code'))->sortable();
        $grid->column('territory', __('Territory'))->sortable();

        // disable actions
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Office::findOrFail($id));

        $show->field('officeCode', __('Code'));
        $show->field('city', __('City'));
        $show->field('phone', __('Phone'));
        $show->field('addressLine1', __('Address 1'));
        $show->field('addressLine2', __('Address 2'));
        $show->field('state', __('State'));
        $show->field('country', __('Country'));
        $show->field('postalCode', __('Zip Code'));
        $show->field('territory', __('Territory'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Office());

        $form->hidden('officeCode', __('OfficeCode'));
        $form->text('city', __('City'))->required('rules');
        $form->mobile('phone', __('Phone'))->required('rules');
        $form->text('addressLine1', __('Address 1'))->required('rules');
        $form->text('addressLine2', __('Address 2'))->required('rules');
        $form->text('state', __('State'))->required('rules');
        $form->text('country', __('Country'))->required('rules');
        $form->text('postalCode', __('Zip Code'))->required('rules');
        $form->text('territory', __('Territory'))->required('rules');

        // before saving
        $form->saving(function (Form $form) {
            $form->officeCode = Office::max('officeCode') + 1;
        });

        return $form;
    }
}
