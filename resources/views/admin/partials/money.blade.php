@if(isset($value) && is_numeric($value))
    ${{ number_format($value,2) }}
@else
    {{$value}}
@endif
